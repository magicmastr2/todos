@extends('app')

@section('content')

<div class="row">
  <div class="col-mid-3">
    <h1>Groups</h1>
    <ul class="list-group">
      @foreach($groups as $currentGroup)
        <li class="list-group-item">
          <a href="{{action('GroupController@show', [$currentGroup->id]) }}">{{ $currentGroup->name }}</a>
        </li>
      @endforeach
    </ul>
        <div class="col-mid-9">
            <a href="{{ action('ToDoController@create', [$group->id]) }}" class="btn btn-default btn-xs pull-right">+</a>
            <h1>{{ $group->name }}</h1>
            <ul class="list-group">
                @foreach($group->ToDos as $todo)
                <li class="list-group-item">{{ $todo->task }} {{ $todo->Due_Date }} {{ $todo->completed }}</li>
            @endforeach
          </ul>
      </div>
  </div>
@endsection